import 'package:flutter/material.dart';

Widget _checkBox(String title, bool value, ValueChanged<bool?>? onChanged) {
  return Container(
      padding: EdgeInsets.only(left: 16,right: 16,bottom: 8),
      child: Column(
        children: [
          Row(
            children: [
              Text(title),
              Expanded(child: Container()),
              Checkbox(value: value, onChanged: onChanged)
            ],
          ),
          Divider()
        ],
      ));
}

class CheckBoxWidget extends StatefulWidget {
  CheckBoxWidget({Key? key}) : super(key: key);

  @override
  _CheckBoxWidgetState createState() => _CheckBoxWidgetState();
}

class _CheckBoxWidgetState extends State<CheckBoxWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CheckBox'),
      ),
      body: Column(
        children: [
          _checkBox('Check 1', check1, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          _checkBox('Check 2', check2, (value) {
            setState(() {
              check2 = value!;
            });
          }),
          _checkBox('Check 3', check3, (value) {
            setState(() {
              check3 = value!;
            });
          }),
          TextButton(
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                        'Check 1 : $check1, Check 2 : $check2, Check 3 : $check3')));
              },
              child: Text('Save'))
        ],
      ),
    );
  }
}
